@extends('layout.master')

@section('judul')
    Detail Cast    
@endsection

@section('content')
    <h1>{{$cast->name}}</h1>
    <p>Umur: {{$cast->umur}}</p>
    <p>Bio: <br>{{$cast->bio}}</p>

    <a href="/cast" class="btn btn-secondary btn-sm">Kembali</a>

@endsection