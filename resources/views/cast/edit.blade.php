@extends('layout.master')

@section('judul')
    Edit Cast    
@endsection

@section('content')
<form action="/cast/{{$cast->id}}" method="POST">
    @csrf
    @method('PUT')
    <div class="form-group">
      <label >Nama</label>
      <input type="text" class="form-control" name='name' value="{{$cast->name}}" placeholder="Enter cast name">
    </div>
    @error('name')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
      <label >Umur</label>
      <input type="text" class="form-control" name='umur' value="{{$cast->umur}}" placeholder="Enter cast age">
    </div>
    @error('umur')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label >Bio</label>
        <textarea name="bio" class="form-control" cols="30" rows="10" placeholder="Enter cast bio">{{$cast->bio}}</textarea>
    </div>
    @error('bio')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
@endsection