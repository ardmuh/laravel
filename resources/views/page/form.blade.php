<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Berlatih HTML</title>
</head>
<body>
    <h1>Buat Account Baru!</h1>
    <h2>Sign Up Form</h2>
    <form action="/welcome" method="POST">
        @csrf
        <label for="fname">First name:</label> <br><br>
        <input type="text" name="fname">
        <br><br>
        <label for="lname">Lasst name:</label> <br><br>
        <input type="text" name="lname">
        <br><br>
        <label name="gender">Gender:</label><br><br>
        <input type="radio" name="gender" >Male <br>
        <input type="radio" name="gender" >Female <br>
        <input type="radio" name="gender" >Other <br><br>
        <label name="natinality">Nationality:</label><br><br>
        <select name="nationality" >
            <option value="indonesia">Indonesia</option>
            <option value="malaysia">Malaysia</option>
            <option value="singapura">Singapura</option>
            <option value="brunai">Brunai Darussalam</option>
        </select> <br><br>
        <label >Language Spoken:</label> <br><br>
        <input type="checkbox" name="bahasa">Bahasa Indonesia <br>
        <input type="checkbox" name="bahasa">English <br>
        <input type="checkbox" name="bahasa">Other <br><br>
        <label>Bio:</label><br><br>
        <textarea name="message" cols="30" rows="10"></textarea>
        <br><br>
        <input type="submit" value="Sign Up">

    </form>

</body>
</html>