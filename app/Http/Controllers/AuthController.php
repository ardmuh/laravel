<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function form()
    {
        return view('page.form');
    }
    public function welcome(Request $request)
    {
        $namaDepan = $request['fname'];
        $namaBelakang = $request['lname'];

        return view('page.welcome' , ['namaDepan' =>$namaDepan, 'namaBelakang' => $namaBelakang]) ;
    }

    // tugas 13
    public function table()
    {
        return view('page.table');
    }

    public function dataTable()
    {
        return view('page.datatabel');
    }
}
